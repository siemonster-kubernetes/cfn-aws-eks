#!/usr/bin/env bash

test -f lambda/lambda.zip
docker run --rm -it -v "$(pwd):/src:ro" -v "$(pwd)/lambda:/out" --user=root --entrypoint /src/.docker/build.sh lambci/lambda:python3.7
mv lambda/lambda.zip lambda/cfn-aws-eks.py_v1.zip
