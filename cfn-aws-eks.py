#!/usr/bin/env python
from crhelper import CfnResource
import boto3
from botocore.exceptions import ClientError
import logging
import random
import string

logger = logging.getLogger(__name__)
# Initialise the helper, all inputs are optional, this example shows the defaults
helper = CfnResource(json_logging=False, log_level='DEBUG', boto_level='CRITICAL')

try:
    session = boto3.session.Session()
    client = boto3.client('eks')
except Exception as e:
    helper.init_failure(e)


def get_properties(event):
    allowed_params = ['name', 'version', 'roleArn', 'resourcesVpcConfig', 'logging', 'clientRequestToken', 'tags',
                      'encryptionConfig']
    return {k: v for k, v in event['ResourceProperties'].items() if k in allowed_params}


@helper.create
def create(event, context):
    logger.info("Got Create")
    # Optionally return an ID that will be used for the resource PhysicalResourceId,
    # if None is returned an ID will be generated. If a poll_create function is defined
    # return value is placed into the poll event as event['CrHelperData']['PhysicalResourceId']
    #
    # To add response data update the helper.Data dict
    # If poll is enabled data is placed into poll event as event['CrHelperData']
    # Grab data from environment
    try:
        properties = get_properties(event)
        response = client.create_cluster(**properties)
        logger.info("Started EKS cluster creation: {}".format(response['cluster']['arn']))

        helper.Data.update({
            'Arn': response['cluster']['arn'],
            'Name': properties['name'],
        })
        return properties['name']
    except Exception as e:
        helper.init_failure(e)


@helper.update
def update(event, context):
    logger.info("Got Update")
    # If the update resulted in a new resource being created, return an id for the new resource. CloudFormation will send
    # a delete event with the old id when stack update completes


@helper.delete
def delete(event, context):
    logger.info("Got Delete")

    properties = get_properties(event)
    response = client.delete_cluster(
        name=properties['name']
    )

    logger.info("EKS cluster destroy initiated: {}".format(response['cluster']['arn']))


@helper.poll_create
def poll_create(event, context):
    logger.info("Got create poll")
    # Return a resource id or True to indicate that creation is complete. if True is returned an id will be generated
    properties = get_properties(event)
    try:
        response = client.describe_cluster(
            name=properties['name']
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            helper.init_failure("Status: FAILED")

    logger.info(response)

    if response['cluster']['status'] == 'ACTIVE':
        helper.Data.update({
            'CertificateAuthorityData': response['cluster']['certificateAuthority']['data'],
            'ClusterSecurityGroupId': response['cluster']['resourcesVpcConfig']['clusterSecurityGroupId'],
            'Endpoint': response['cluster']['endpoint'],
        })
        return True

    if  response['cluster']['status'] == 'FAILED':
        helper.init_failure("Status: FAILED")


@helper.poll_delete
def poll_delete(event, context):
    logger.info("Got delete poll")

    properties = get_properties(event)
    try:
        response = client.describe_cluster(
            name=properties['name']
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            return True

    logger.info(response)


def handler(event, context):
    helper(event, context)
